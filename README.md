# README #

This README would normally document whatever steps are necessary to get your application up and running.

Cette application Android utilise le mode P2P NFC (Android Beam) afin de permettra à des personnes de se partager leurs cartes de visites en utilisant leur tépéphone Android NFC. 
L'objectif est de permettre à un utilisateur d'éditer sa carte de visite (Nom, Prénom, Email, Téléphone, Adresse, Titre, ...) 
puis de les envoyer à un autre utilisateur en utilisant la technologie Beam. 
Ainsi en plus du partage de carte de visite, l'application  permet à l'utilisateur de créer sa carte de visite à partir du repertoire du téléphone. 
L'utilisateur peut visualiser les cartes de visites envoyées et/ou recues. 