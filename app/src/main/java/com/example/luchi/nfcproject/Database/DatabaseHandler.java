package com.example.luchi.nfcproject.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.SQLException;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.luchi.nfcproject.Models.Carte;


public class DatabaseHandler extends SQLiteOpenHelper{

    public static  final String DATABASE_NAME = "CarteVisite_db";
    public static  final int DATABASE_VERSION = 2;
    public static final String TABLE_NAME = "Cartes";
    public static final String KEY = "id";
    public static final String PRENOM ="prenom";
    public static final String NOM = "nom";
    public static final String ADRESSE = "adresse";
    public static final String TEL ="telephone";
    public static final String TITRE = "titre";
    public static final String EMAIL = "email";

    private static final String TABLE_CREATE_CARTE = "CREATE TABLE " + TABLE_NAME + "(" + KEY + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
            PRENOM + "TEXT, " + NOM + "TEXT , " + ADRESSE + "TEXT, " +
            TEL + "TEXT ," + TITRE + "TEXT,"+ EMAIL+" TEXT);";
    private static final String TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    private SQLiteDatabase db;
    private Context context;
    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    public DatabaseHandler open(){
        DatabaseHandler   dbHelper = new DatabaseHandler(context, DATABASE_NAME,null,DATABASE_VERSION);
        try{
            db = dbHelper.getWritableDatabase();

        }
        catch (SQLException e){
            db = dbHelper.getReadableDatabase();

        }
        return this;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE_CARTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TABLE_DROP);

        onCreate(db);
    }

    // inserer une Carte  dans la base de donnees
    public void ajouterCarte(Carte carte){
        ContentValues values = new ContentValues();
        values.put(PRENOM,carte.getPrenom());
        values.put(NOM,carte.getNom());
        values.put(ADRESSE,carte.getAdresse());
        values.put(EMAIL,carte.getEmail());
        values.put(TITRE,carte.getTitre());
        values.put(TEL,carte.getTelephone());


         db.insert(TABLE_NAME,null,values);
    }

    // modifier une Carte  de la base de donnees
    public void modifierCarte(Carte carte){
        ContentValues values = new ContentValues();
        values.put(PRENOM,carte.getPrenom());
        values.put(NOM,carte.getNom());
        values.put(ADRESSE,carte.getAdresse());
        values.put(EMAIL,carte.getEmail());
        values.put(TITRE,carte.getTitre());
        values.put(TEL,carte.getTelephone());

         db.update(TABLE_NAME,values,"id = " + carte.getId(),null) ;
    }

    // supprimer une Carte  de la base de donnees
    public boolean deleteCard(String table, long id){

        return db.delete(table,"id = " + id,null) > 0;
    }

    // get all cards from database (from specific table)
    public Cursor getAllCarteFromDB(){

        Cursor cartesCursor;
        String [] columns = {KEY,PRENOM,NOM,ADRESSE,TEL,TITRE,EMAIL};
        cartesCursor = db.query(TABLE_NAME,columns,null,null,null,null,null);
        return cartesCursor;
    }
}
