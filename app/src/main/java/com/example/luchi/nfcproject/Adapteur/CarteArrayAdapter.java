package com.example.luchi.nfcproject.Adapteur;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Movie;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.luchi.nfcproject.Models.Carte;
import com.example.luchi.nfcproject.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class CarteArrayAdapter extends ArrayAdapter<Carte> {
    /**
     * Constructor.  Class Array is not instantiable.
     */

    public CarteArrayAdapter(Context context, ArrayList<Carte> cartes) {

        super(context, 0, cartes);

    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Carte carte = getItem(position);

        if(convertView == null)
        {

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_carte, parent , false);


        }


        TextView txtNom = (TextView) convertView.findViewById(R.id.nomValeur);
        TextView txtPrenom = (TextView) convertView.findViewById(R.id.prenomValeur);
        TextView txtTel = (TextView) convertView.findViewById(R.id.telValeur);

        int orientation = getContext().getResources().getConfiguration().orientation;
        txtNom.setText(carte.getNom());
        txtPrenom.setText(carte.getPrenom());
        txtTel.setText(carte.getTelephone());
        return convertView;
    }
}
