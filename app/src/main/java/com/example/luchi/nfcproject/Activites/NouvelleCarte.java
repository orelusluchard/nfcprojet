package com.example.luchi.nfcproject.Activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.luchi.nfcproject.Database.DatabaseHandler;
import com.example.luchi.nfcproject.Models.Carte;
import com.example.luchi.nfcproject.R;

public class NouvelleCarte extends AppCompatActivity {
    private DatabaseHandler dbHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nouvelle_carte);

        dbHandler = new DatabaseHandler(getApplicationContext(), "CarteVisite_db", null, 2);
        dbHandler.open();
    }

    public void ajouterCarte(View view) {
        TextView tvPrenom = (TextView) findViewById(R.id.prenomValeur);
        EditText tvNom = (EditText) findViewById(R.id.nomValeur);
        EditText tvTel = (EditText) findViewById(R.id.telValeur);
        EditText tvTitre = (EditText) findViewById(R.id.titreValeur);
        EditText tvAdresse = (EditText) findViewById(R.id.adresseValeur);
        EditText tvEmail = (EditText) findViewById(R.id.emailValeur);
String email = tvEmail.getText().toString();
String prenom = tvPrenom.getText().toString();
String nom = tvNom.getText().toString();
String adresse = tvAdresse.getText().toString();
String titre = tvTitre.getText().toString();
String tel = tvTel.getText().toString();
        Carte carte = new Carte(prenom, nom , adresse);
        carte.setEmail(email);
        carte.setTitre(titre);
        carte.setTelephone(tel);
dbHandler.ajouterCarte(carte);
    }
}
