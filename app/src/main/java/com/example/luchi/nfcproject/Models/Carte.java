package com.example.luchi.nfcproject.Models;

import java.io.Serializable;

public class Carte implements Serializable {
    private long id;
    private String nom;
    private String prenom;
    private String adresse;
    private String email;
    private String titre;
    private String telephone;


    public Carte(String nom, String prenom, String adresse ){
this.nom = nom;
this.prenom = prenom;
this.adresse = adresse;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Carte(Long id, String nom, String prenom, String adresse , String email , String titre, String telephone){
        this(nom, prenom , adresse);
        this.id = id;
        this.email = email;
        this.telephone = telephone;
        this.titre = titre;

    }
    public Carte(String nom, String prenom, String adresse , String email , String titre, String telephone){
        this(nom, prenom , adresse);
        this.email = email;
        this.telephone = telephone;
        this.titre = titre;

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
