package com.example.luchi.nfcproject.Activites;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.graphics.Movie;
import android.nfc.NfcAdapter;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.luchi.nfcproject.Adapteur.CarteArrayAdapter;
import com.example.luchi.nfcproject.Database.DatabaseHandler;
import com.example.luchi.nfcproject.Models.Carte;
import com.example.luchi.nfcproject.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.io.FileUtils;
public class CarteActivity extends AppCompatActivity {
    ArrayList<Carte> cartes;
    CarteArrayAdapter carteArrayAdapter;
    private DatabaseHandler dbHandler;
    ListView lvCartes;
    private NfcAdapter nfcAdapter;
    private PendingIntent nfcPendingIntent;
    private IntentFilter[] ndefIntentFilters;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carte);

        dbHandler = new DatabaseHandler(getApplicationContext(), "CarteVisite_db", null, 2);
        dbHandler.open();
        lvCartes = (ListView)findViewById(R.id.lvItems);
      //  lvCartesLand
        Carte carte = new Carte("Orelus","Luchard","christ-roi rue lamartine");
        carte.setTelephone("36585529");
        //dbHandler.ajouterCarte(carte);
        cartes = new ArrayList<Carte>();
        cartes.add(carte);

       getCardsFromDatabase();
       //   int orientation = getResources().getConfiguration().orientation;

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this,0,new Intent(this,getClass()).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter ndefFilter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndefFilter.addDataType("text/plain");                   // set payload type which interests us in NDEF message
            ndefIntentFilters = new IntentFilter[]{ndefFilter};
        }
        catch (IntentFilter.MalformedMimeTypeException e) {
            e.printStackTrace();
        }
        carteArrayAdapter = new CarteArrayAdapter(CarteActivity.this , cartes);
        lvCartes.setAdapter(carteArrayAdapter);


         setupViewListener();
        setupListViewListener();
    }

    private void setupViewListener(){
        lvCartes.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {


                    public void onItemClick(AdapterView<?> arg0,
                                            View arg1, int arg2, long arg3) {

                        String nom =    cartes.get(arg2).getNom();
                        String prenom =  cartes.get(arg2).getPrenom();
                        String adresse = cartes.get(arg2).getAdresse();

                        Carte carte = new Carte(nom , prenom , adresse);
carte.setTelephone("36585529");
carte.setTitre("Inforamticien");
carte.setEmail("luchill@yahoo.fr");
                        launchComposeViewCarte(carte);


                    }
                });
    }

    private void setupListViewListener(){
        lvCartes.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                        String nom =    cartes.get(i).getNom();
                        String prenom =  cartes.get(i).getPrenom();
                        String adresse = cartes.get(i).getAdresse();

                        Carte carte = new Carte(nom , prenom , adresse);
                        carte.setTelephone("36585529");
                        carte.setTitre("Inforamticien");
                        carte.setEmail("luchill@yahoo.fr");
                        launchEditViewCarte(carte);
                        return true;
                    }
                }
        );
    }

    public void launchComposeViewCarte(Carte carte) {

        Intent i = new Intent(CarteActivity.this, DetailCarteActivity.class);
        i.putExtra("carte",carte);
        startActivity(i); // brings up the second activity

    }

    public void launchEditViewCarte(Carte carte) {

        Intent i = new Intent(CarteActivity.this, ModifierCarte.class);
        i.putExtra("carte",carte);
        startActivity(i); // brings up the second activity

    }

    private void getCardsFromDatabase(){
        Cursor cardCursor = dbHandler.getAllCarteFromDB();

        if (cardCursor != null && cardCursor.getCount() > 0) {
            cardCursor.moveToFirst();
            do{
                long id = cardCursor.getLong(0);
                String prenom = cardCursor.getString(1);
                String nom = cardCursor.getString(2);
                String adresse = cardCursor.getString(3);
                String tel = cardCursor.getString(4);
                String titre = cardCursor.getString(5);
                String email = cardCursor.getString(6);


                Carte carte = new Carte(nom, prenom, adresse , email ,  titre, tel);
                carte.setId(id);
                cartes.add(carte);
            }
            while (cardCursor.moveToNext());
        }
        else {

        }

    }

    private boolean checkIfNfcAvailable(){
        // if device not support NFC - display information about this to the user
        if (nfcAdapter == null) {
            Toast.makeText(this,"NFC not supported at your device!",Toast.LENGTH_SHORT).show();
            finish();
            return false;
        }
        // if device support NFC but it's disable at the moment - go to NFC Settings
        else if (!nfcAdapter.isEnabled()){
            Toast.makeText(this,"Turn on NFC.",Toast.LENGTH_LONG).show();

            Intent paramNFC = new Intent(Settings.ACTION_NFC_SETTINGS);
            startActivity(paramNFC);
            return false;
        }
        // if user choose to share Card via NFC / write to tag while NFC is on - display that NFC is on
        else {
            Toast.makeText(this,"NFC is on.",Toast.LENGTH_SHORT).show();
            finish();
            return true;
        }
    }

    public void AjouterContact(){
        Intent i = new Intent(CarteActivity.this, NouvelleCarte.class);
        startActivity(i); // brings up the second activity
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_carte, menu);
        MenuItem item = menu.findItem(R.id.action_add);

        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                AjouterContact();
                return true;
            }
        });

        MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            public boolean onQueryTextSubmit(String query) {
                // perform query here

                // workaround to avoid issues with some emulators and keyboard devices firing twice if a keyboard enter is used
                // see https://code.google.com/p/android/issues/detail?id=24599

                searchView.clearFocus();



                return true;
            }


            public boolean onQueryTextChange(String newText) {
                // fetchArticles(newText);
                return false;
            }
        });
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }


}
