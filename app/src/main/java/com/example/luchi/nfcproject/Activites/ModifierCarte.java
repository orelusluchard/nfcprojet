package com.example.luchi.nfcproject.Activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.luchi.nfcproject.Adapteur.CarteArrayAdapter;
import com.example.luchi.nfcproject.Database.DatabaseHandler;
import com.example.luchi.nfcproject.Models.Carte;
import com.example.luchi.nfcproject.R;

import android.view.View;
import android.widget.TextView;
import android.widget.EditText;

public class ModifierCarte extends AppCompatActivity {
private Carte mContact;
    private DatabaseHandler dbHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifier_carte);
        mContact = (com.example.luchi.nfcproject.Models.Carte)getIntent().getExtras().getSerializable("carte");

        TextView tvPrenom = (TextView) findViewById(R.id.prenomValeur);
        EditText tvNom = (EditText) findViewById(R.id.nomValeur);
        EditText tvTel = (EditText) findViewById(R.id.telValeur);
        EditText tvTitre = (EditText) findViewById(R.id.titreValeur);
        EditText tvAdresse = (EditText) findViewById(R.id.adresseValeur);
        EditText tvEmail = (EditText) findViewById(R.id.emailValeur);
        tvPrenom.setText(mContact.getPrenom());
        tvNom.setText(mContact.getNom());
        tvTel.setText(mContact.getTelephone());
        tvTitre.setText(mContact.getTitre());
        tvAdresse.setText(mContact.getAdresse());
        tvEmail.setText(mContact.getEmail());

        dbHandler = new DatabaseHandler(getApplicationContext(), "CarteVisite_db", null, 2);
        dbHandler.open();


    }

    public void modifierCarte(View view) {
        TextView tvPrenom = (TextView) findViewById(R.id.prenomValeur);
        EditText tvNom = (EditText) findViewById(R.id.nomValeur);
        EditText tvTel = (EditText) findViewById(R.id.telValeur);
        EditText tvTitre = (EditText) findViewById(R.id.titreValeur);
        EditText tvAdresse = (EditText) findViewById(R.id.adresseValeur);
        EditText tvEmail = (EditText) findViewById(R.id.emailValeur);

        String email = tvEmail.getText().toString();
        String prenom = tvPrenom.getText().toString();
        String nom = tvNom.getText().toString();
        String adresse = tvAdresse.getText().toString();
        String titre = tvTitre.getText().toString();
        String tel = tvTel.getText().toString();

        mContact.setTelephone(tel);
        mContact.setTitre(titre);
        mContact.setEmail(email);
        mContact.setNom(nom);
        mContact.setPrenom(prenom);
        mContact.setAdresse(adresse);
        dbHandler.modifierCarte(mContact);
    }
}
