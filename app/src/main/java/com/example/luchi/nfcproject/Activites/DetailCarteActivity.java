package com.example.luchi.nfcproject.Activites;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Movie;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.luchi.nfcproject.Models.Carte;
import com.example.luchi.nfcproject.R;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailCarteActivity extends AppCompatActivity {
    private Carte carte;
    private NfcAdapter nfcAdapter;
    private PendingIntent nfcPendingIntent;
    private IntentFilter[] ndefIntentFilters;

    private boolean mWriteMode = false;
    private IntentFilter[] mWriteTagFilters;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_carte);


         carte = (Carte) getIntent().getSerializableExtra("carte");

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this,0,new Intent(this,getClass()).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter ndefFilter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndefFilter.addDataType("text/plain");                   // set payload type which interests us in NDEF message
            ndefIntentFilters = new IntentFilter[]{ndefFilter};
        }
        catch (IntentFilter.MalformedMimeTypeException e) {
            e.printStackTrace();
        }
        Context context = this ;
        TextView tvPrenom = (TextView) findViewById(R.id.prenomValeur);
        TextView tvNom = (TextView) findViewById(R.id.nomValeur);
        TextView tvTel = (TextView) findViewById(R.id.telValeur);
        TextView tvTitre = (TextView) findViewById(R.id.titreValeur);
        TextView tvAdresse = (TextView) findViewById(R.id.adresseValeur);
        TextView tvEmail = (TextView) findViewById(R.id.emailValeur);
        tvPrenom.setText(carte.getPrenom());
        tvNom.setText(carte.getNom());
        tvTel.setText(carte.getTelephone());
        tvTitre.setText(carte.getTitre());
        tvAdresse.setText(carte.getAdresse());
       // tvEmail.setText(carte.getEmail());

        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        mWriteTagFilters = new IntentFilter[]{tagDetected};
    }

    @Override
    public void onResume(){
        super.onResume();
        // if NFC is enable on device set NDEF message ready for sharing via NFC
        if (nfcAdapter != null && nfcAdapter.isEnabled()) {
            nfcAdapter.setNdefPushMessage(CarteNdefMessage(), this);
        }

    }
    private NdefMessage CarteNdefMessage(){

        byte[] informations_carte = carteToJSON(carte).getBytes();
        NdefRecord record1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,NdefRecord.RTD_TEXT,new byte[0],informations_carte);
        NdefMessage msg = new NdefMessage(new NdefRecord[]{record1});
        return  msg;
    }

    private String carteToJSON(Carte carte){

        try {
            JSONObject cardJSON = new JSONObject();
            cardJSON.put("prenom",carte.getPrenom());
            cardJSON.put("nom",carte.getNom());
            cardJSON.put("adresse",carte.getAdresse());
            cardJSON.put("tel",carte.getTelephone());
            cardJSON.put("titre",carte.getTitre());
            cardJSON.put("email",carte.getEmail());




            return cardJSON.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    private Carte getCarteFromNdefMessage(Intent intent){
        Carte carte = null;
        NdefMessage[] msgs = null;
        Parcelable[] rawMsg = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        if (rawMsg != null){
            msgs = new NdefMessage[rawMsg.length];
            for (int i = 0; i < rawMsg.length; i++){
                msgs[i] = (NdefMessage)rawMsg[i];
            }
        }

        if (msgs[0].getRecords().length == 3){
            carte = getCarteFromJSON(msgs[0].getRecords()[0].getPayload());

        }

        else if (msgs[0].getRecords().length == 2){
            carte = getCarteFromJSON(msgs[0].getRecords()[0].getPayload());
            carte.setId(0);
        }

        else{
            carte = new Carte("prenom","nom","adresse");
            carte.setId(0);
        }


        return carte;
    }



    private Carte getCarteFromJSON(byte[] carte_infos){

        Carte carte = null;
        try{
            JSONObject cardJSON = new JSONObject(new String(carte_infos));
            String prenom = cardJSON.getString("prenom");
            String nom = cardJSON.getString("nom");
            String adresse = cardJSON.getString("adresse");
            String tel = cardJSON.getString("tel");
            String titre = cardJSON.getString("titre");
            String email = cardJSON.getString("email");


            carte = new Carte(prenom, nom, adresse);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return carte;
    }
}
